package main

import (
	"fmt"
	"os"
	"strconv"

	"exercice.go/pendu/dictionnaire"
	"exercice.go/pendu/pendu"
	"exercice.go/pendu/score"
)

func main() {
	meilleureScore, e := score.Charger("score.txt")

	if e != nil {
		fmt.Printf("Impossible de charger le meilleur score : %v\n", e)
		os.Exit(1)
	}

	err := dictionnaire.Charger("mots.txt")

	if err != nil {
		fmt.Printf("Impossible de charger le dictionnaire : %v\n", err)
		os.Exit(1)
	}

	j, _ := pendu.New(8, dictionnaire.ChoisirMot())

	pendu.MsgBienvenue(meilleureScore)

	proposition := ""
	for {
		pendu.Dessin(j, proposition)

		switch j.Etat {
		case "gagne", "perdu":
			parse, _ := strconv.Atoi(meilleureScore)
			if j.Score > parse {
				score.Modifier("score.txt", strconv.Itoa(j.Score))
			}
			os.Exit(0)
		}

		l, err := pendu.LectureSaisie()
		if err != nil {
			fmt.Printf("Erreur : %v", err)
			os.Exit(1)
		}
		proposition = l
		j.Hypothese(proposition)
	}

}

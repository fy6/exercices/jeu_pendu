package dictionnaire

import (
	"bufio"
	"math/rand"
	"os"
	"time"
)

var mots = make([]string, 0, 50)

func Charger(fichier string) error {
	f, err := os.Open(fichier)

	if err != nil {
		return err
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		mots = append(mots, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return err
	}
	return nil
}

func ChoisirMot() string {
	rand.Seed(time.Now().Unix())
	i := rand.Intn(len(mots))
	return mots[i]
}

package pendu

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var lecteur = bufio.NewReader(os.Stdin)

func LectureSaisie() (proposition string, err error) {
	valid := false
	for !valid {
		fmt.Print("\n\n\nQuelle est votre proposition ? ('?' => INDICE): ")
		proposition, err = lecteur.ReadString('\n')
		if err != nil {
			return proposition, err
		}
		proposition = strings.TrimSpace(proposition)
		if len(proposition) != 1 {
			fmt.Printf("Saisie Invalide. lettre=%v, taille=%v\n", proposition, len(proposition))
			continue
		}
		valid = true
	}
	return proposition, nil
}

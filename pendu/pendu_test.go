package pendu

import "testing"

func TestLettreOK(t *testing.T) {
	mot := []string{"b", "o", "b"}
	proposition := "b"
	ok := lettreOK(proposition, mot)
	if !ok {
		t.Errorf("Le mot %s contient la lettre %s. %v", mot, proposition, ok)
	}
}

func TestLettreKO(t *testing.T) {
	mot := []string{"b", "o", "b"}
	proposition := "a"
	ok := lettreOK(proposition, mot)
	if ok {
		t.Errorf("Le mot %s ne contient pas la lettre %s. %v", mot, proposition, ok)
	}
}

func TestMotInvalid(t *testing.T) {
	_, err := New(3, "")
	if err == nil {
		t.Errorf("Devrait retourner une erreur lorsqu'on utilise un mot invalid ( < 2 caratères)")
	}
}

func TestBonneReponse(t *testing.T) {
	j, _ := New(3, "bob")
	j.Hypothese("b")
	etatValid(t, "bonneRep", j.Etat)
}

func TestMauvaiseReponse(t *testing.T) {
	j, _ := New(3, "bob")
	j.Hypothese("a")
	etatValid(t, "mauvaiseRep", j.Etat)
}

func TestPerdu(t *testing.T) {
	j, _ := New(3, "bob")
	j.Hypothese("a")
	j.Hypothese("e")
	j.Hypothese("i")
	etatValid(t, "perdu", j.Etat)
}

func TestVictoire(t *testing.T) {
	j, _ := New(3, "bob")
	j.Hypothese("b")
	j.Hypothese("o")
	j.Hypothese("b")
	etatValid(t, "gagne", j.Etat)
}

func TestPropositionDejaFaite(t *testing.T) {
	j, _ := New(3, "bob")
	j.Hypothese("b")
	j.Hypothese("b")
	etatValid(t, "dejaUtilisee", j.Etat)
}

func etatValid(t *testing.T, etatAttendu, etat string) bool {
	if etatAttendu != etat {
		t.Errorf("l'etat devrait être '%v'. Obtenu=%v", etatAttendu, etat)
		return false
	}
	return true
}

func TestCalculScore(t *testing.T) {
	j1, _ := New(1, "bob")
	j1.Hypothese("b")
	j1.Hypothese("o")

	j2, _ := New(2, "bob")
	j2.Hypothese("b")
	j2.Hypothese("?")
	j2.Hypothese("o")

	j3, _ := New(1, "bob")
	j3.Hypothese("o")
	j3.Hypothese("a")

	if j1.Score != 20 {
		t.Errorf("Devrait retourner un score de 20 => %v", j1.Score)
	}
	if j2.Score != 13 {
		t.Errorf("Devrait retourner un score de 20 => %v", j2.Score)
	}
	if j3.Score != 0 {
		t.Errorf("Devrait retourner un score null => %v", j3.Score)
	}
}

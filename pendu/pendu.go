package pendu

import (
	"fmt"
	"strings"
)

type Jeu struct {
	Etat            string   // état du jeu
	Lettres         []string // lettre du mot à trouver
	LettresTrouvees []string // Bonne réponse
	LettreUtilisees []string //Toute les propositions
	Tour            int      // Nombre d'essai
	Score           int      // Score de la partie
	Indice          int      //Nombre d'indice
}

func New(tours int, mot string) (*Jeu, error) {
	if len(mot) < 2 {
		return nil, fmt.Errorf("le mot '%s' doit faire au moins 2 caratères", mot)
	}
	lettres := strings.Split(strings.ToUpper(mot), "")
	trouvees := make([]string, len(lettres))
	for i := 0; i < len(lettres); i++ {
		trouvees[i] = "_"
	}

	j := &Jeu{
		Etat:            "",
		Lettres:         lettres,
		LettresTrouvees: trouvees,
		LettreUtilisees: []string{},
		Tour:            tours,
		Score:           0,
		Indice:          2,
	}
	return j, nil
}

func (j *Jeu) Hypothese(proposition string) {
	switch j.Etat {
	case "gagne", "perdu":
		return
	}

	if proposition == "?" {
		if j.Indice > 0 {
			j.Score -= 7
			indice := obtenirIndice(j)
			fmt.Println("-7pts pour cet indice :")
			fmt.Printf("### %s ###", indice)
			j.Indice--
		} else {
			fmt.Println("Vous n'avez plus d'indice ! :'(")
		}

	} else {
		proposition = strings.ToUpper(proposition)

		if lettreOK(proposition, j.LettreUtilisees) {
			j.Etat = "dejaUtilisee"
			j.Score -= 2
		} else if lettreOK(proposition, j.Lettres) {
			j.Etat = "bonneRep"
			j.Score += 10
			j.revelerLettre(proposition)
			if victoire(j.Lettres, j.LettresTrouvees) {
				j.Etat = "gagne"
			}
		} else {
			j.Etat = "mauvaiseRep"
			j.Score -= 5
			j.mauvaisTour(proposition)

			if j.Tour <= 0 {
				j.Etat = "perdu"
				j.Score = 0
			}
		}
	}
}

func lettreOK(proposition string, lettres []string) bool {
	for _, l := range lettres {
		if l == proposition {
			return true
		}
	}
	return false
}

func obtenirIndice(j *Jeu) string {
	//fmt.Println(j.LettresTrouvees)
	//fmt.Println(j.Lettres)
	index := 0
	for i := 0; i < len(j.LettresTrouvees); i++ {
		if j.LettresTrouvees[i] == "_" {
			index = i
			break
		}
	}
	return j.Lettres[index]
}

func (j *Jeu) revelerLettre(proposition string) {
	j.LettreUtilisees = append(j.LettreUtilisees, proposition)
	for i, l := range j.Lettres {
		if l == proposition {
			j.LettresTrouvees[i] = proposition
		}
	}
}

func (j *Jeu) mauvaisTour(proposition string) {
	j.Tour--
	j.LettreUtilisees = append(j.LettreUtilisees, proposition)
}

func victoire(lettres []string, lettresTrouvees []string) bool {
	for i := range lettres {
		if lettres[i] != lettresTrouvees[i] {
			return false
		}
	}
	return true
}

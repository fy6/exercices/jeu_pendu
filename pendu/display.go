package pendu

import "fmt"

func MsgBienvenue(score string) {
	fmt.Printf(` 
	   __ ____ _  _    ____ _  _    ____ ____ __ _ ____ _  _    _   
	 _(  (  __/ )( \  (    / )( \  (  _ (  __(  ( (    / )( \  / \  
   	/ \) \) _)) \/ (   ) D ) \/ (   ) __/) _)/    /) D ) \/ (  \_/  
   	\____(____\____/  (____\____/  (__) (____\_)__(____\____/  (_)  
   
	MEILLEUR SCORE : %v   `, score)
}

func Dessin(j *Jeu, proposition string) {
	dessinerEchafaud(j.Tour)
	dessinEtat(j, proposition)
}

func dessinEtat(j *Jeu, proposition string) {
	fmt.Printf("Tentatives: %d\t Restantes: %d\t Indices:%d\n", len(j.LettreUtilisees), j.Tour, j.Indice)

	fmt.Print("Trouvées: ")
	dessinerLettres(j.LettresTrouvees)

	fmt.Print("Utilisées: ")
	dessinerLettres(j.LettreUtilisees)

	switch j.Etat {
	case "bonneRep":
		fmt.Println("Bonne réponse ! +10pts")
	case "dejaUtilisee":
		fmt.Printf("La lettre '%s' a déjà été utilisée ! -2pts\n", proposition)
	case "mauvaiseRep":
		fmt.Printf("La lettre '%s' n'est pas dans le mot ! -5pts\n", proposition)
	case "perdu":
		fmt.Printf("La lettre '%s' n'est pas dans le mot ! -5pts\n", proposition)
		fmt.Print("Vous avez perdu ! :( le mot été : ")
		dessinerLettres(j.Lettres)
		fmt.Printf("SCORE = %d\n", j.Score)
	case "gagne":
		fmt.Println("Bonne réponse ! +10pts")
		fmt.Println("Vous avez gagné ! :)")
		fmt.Printf("SCORE = %d\n", j.Score)
	}
}

func dessinerLettres(l []string) {
	for _, c := range l {
		fmt.Printf("%v ", c)
	}
	fmt.Println()
}

func dessinerEchafaud(l int) {
	var dessin string
	switch l {
	case 0:
		dessin = `
    ____
   |    |      
   |    o      
   |   /|\     
   |    |
   |   / \
  _|_
 |   |______
 |          |
 |__________|
		`
	case 1:
		dessin = `
    ____
   |    |      
   |    o      
   |   /|\     
   |    |
   |    
  _|_
 |   |______
 |          |
 |__________|
		`
	case 2:
		dessin = `
    ____
   |    |      
   |    o      
   |    |
   |    |
   |     
  _|_
 |   |______
 |          |
 |__________|
		`
	case 3:
		dessin = `
    ____
   |    |      
   |    o      
   |        
   |   
   |   
  _|_
 |   |______
 |          |
 |__________|
		`
	case 4:
		dessin = `
    ____
   |    |      
   |      
   |      
   |  
   |  
  _|_
 |   |______
 |          |
 |__________|
		`
	case 5:
		dessin = `
    ____
   |        
   |        
   |        
   |   
   |   
  _|_
 |   |______
 |          |
 |__________|
		`
	case 6:
		dessin = `
    
   |     
   |     
   |     
   |
   |
  _|_
 |   |______
 |          |
 |__________|
		`
	case 7:
		dessin = `
  _ _
 |   |______
 |          |
 |__________|
		`
	case 8:
		dessin = `

		`
	}
	fmt.Println(dessin)
}

package score

import (
	"bufio"
	"os"
)

var meilleurScore []string

func Charger(fichier string) (string, error) {
	f, err := os.Open(fichier)

	if err != nil {
		return "0", err
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		meilleurScore = append(meilleurScore, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return "0", err
	}

	if len(meilleurScore) == 0 {
		return "0", err
	}
	return meilleurScore[0], nil
}

func Modifier(fichier string, score string) {

	editFichier, err := os.Create(fichier)
	if err != nil {
		return
	}
	defer editFichier.Close()
	writter := bufio.NewWriter(editFichier)
	writter.WriteString(score)
	defer writter.Flush()
}
